# Twitch Together

Twitch Together is app inspired by [multitwitch](https://github.com/bhamrick/multitwitch) that allows users to watch multiple twitch.tv streams at once. View the live site [here](https://twitchtogether.com/).

## Built With

* [React](https://reactjs.org/docs/getting-started.html)
* [styled-components](https://styled-components.com/docs)
* [styled-icons](https://styled-icons.js.org/)
* [react-spring](https://www.react-spring.io/docs/hooks/basics)
* [react-grid-layout](https://github.com/react-grid-layout/react-grid-layout#readme)
* [react-sizeme](https://github.com/ctrlplusb/react-sizeme#readme)
