import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from './store';
import { GlobalStyles } from './globals';
import App from './App';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <GlobalStyles/>
    <Provider>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();
