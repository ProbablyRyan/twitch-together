import styled from 'styled-components';

const optionStyles = {
  fontFamily: "'Roboto Condensed', sans-serif",
  fontSize: "90%"
};

const StyledAnchor = styled.a`
  & > * {
    font-family: ${ props => props.theme.fontFamily };
    font-size: ${ props => props.theme.fontSize };
  }
`;

export function NavAnchor({ children, href }) {
  return (
    <StyledAnchor
      theme={ optionStyles }
      href={ href }
      target="_blank" 
      rel="noopener noreferrer"
    >
      { children }
    </StyledAnchor>
  );
}

const StyledButton = styled.button`
  & > * {
    font-family: ${ props => props.theme.fontFamily };
    font-size: ${ props => props.theme.fontSize };
  }
`;

export function NavButton({ children, onClick }) {
  return (
    <StyledButton
      theme={ optionStyles }
      onClick={ onClick }
    >
      { children }
    </StyledButton>
  );
}