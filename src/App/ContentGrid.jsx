import { useContext } from "react";
import styled from 'styled-components';
import { config, useTransition } from "react-spring";
import { store } from "../store";
import StreamGrid from './ContentGrid/StreamGrid';
import ChatPanel from './ContentGrid/ChatPanel';

const FlexWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  height: 90vh;
  width: 100%;
  overflow: hidden;
`;

export default function ContentGrid() {
  const { chatPanelIsVisible } = useContext(store).state;
  const chatPanelTransition = useTransition(
    chatPanelIsVisible,
    null,
    {
      config: config.stiff,
      initial: {
        opacity: 1
      },
      from: {
        opacity: 0
      },
      enter: {
        opacity: 1
      },
      leave: {
        opacity: 0
      }
    }
  );
  
  return (
    <FlexWrapper
    >
      <StreamGrid>
        Hello
      </StreamGrid>
      {
        chatPanelTransition.map(({ item, key, props }) => 
          item && (<ChatPanel key={ key } transition={ props }/>)
        )
      }
    </FlexWrapper>
  );
}