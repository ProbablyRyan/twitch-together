import styled from 'styled-components';
import { Chat2, RemoteControl2 } from '@styled-icons/remix-line';
import { CodeSlash } from '@styled-icons/bootstrap';
import { Donate } from '@styled-icons/fa-solid';
import { EyeSlash } from '@styled-icons/bootstrap';
import { NavAnchor, NavButton } from './BottomNav/navOptions';
import { baseElevation2, baseElevation3, highlitGray } from '../globals';
import { useContext } from 'react';
import { store } from '../store';
import * as packageJSON from '../../package.json';

const StyledNav = styled.div`
  background-color: ${ baseElevation2 };
  bottom: 0;
  display: flex;
  height: 10vh;
  justify-content: space-evenly;
  position: fixed;
  width: 100%;
  will-change: transform;

  & > * {
    align-items: center;
    background-color: ${ baseElevation3 };
    border: 0.2rem solid ${ baseElevation3 };
    border-radius: 0;
    color: white;
    cursor: pointer;
    display: flex;
    flex: 1 1 25%;
    flex-direction: column;
    font-size: 1.25em;
    font-family: 'Roboto Condensed', sans-serif;
    justify-content: space-evenly;
    outline: none;
    text-decoration: none;
  }

  & > *:hover,
  & > *:focus {
    border-color: ${ highlitGray };
  }

  & > *:active {
    background-color: ${ highlitGray };
    border-color: ${ highlitGray };
  }
`;

export default function BottomNav() {
  const { state, dispatch } = useContext(store);
  const { streams, chatPanelIsVisible, streamManagerIsVisible } = state;
  
  const blurAndDispatch = toDispatch => e => {
    e.target.blur();
    dispatch(toDispatch);
  };

  const blurAndToggleChatPannel = blurAndDispatch({ type: 'TOGGLE_CHAT_PANEL' });
  const blurAndShowStreamManager = blurAndDispatch({ type: 'SET_STREAM_MANAGER', payload: true });

  return (
    <StyledNav>
      <NavAnchor
        href={ process.env.REACT_APP_SUPPORT_LINK || 'https://ko-fi.com/' }
      >
        <Donate size="25"/>
        <p>Support</p>
      </NavAnchor>
      <NavAnchor
        href={ packageJSON.repository.url || 'https://www.gitlab.com' }
      >
        <CodeSlash size="25"/>
        <p>Contribute</p>
      </NavAnchor>
      {
        (!chatPanelIsVisible || streamManagerIsVisible) && (
          <NavButton
            onClick={ e => {
              e.target.blur();
              if (streams.length > 0) {
                dispatch({ type: 'SET_STREAM_MANAGER', payload: false });
              }
            }}
          >
            <Chat2 size="25"/>
            <p>Show Chat</p>
          </NavButton>
        )
      }
      {
        (!chatPanelIsVisible || !streamManagerIsVisible) && (
          <NavButton
            onClick={ blurAndShowStreamManager }
          >
            <RemoteControl2 size="25"/>
            <p>Control Panel</p>
          </NavButton>
        )
      }
      {
        chatPanelIsVisible && (
          <NavButton
            onClick={ blurAndToggleChatPannel }
          >
            <EyeSlash size="25"/>
            <p>Hide Sidebar</p>
          </NavButton>
        )
      }
    </StyledNav>
  );
}