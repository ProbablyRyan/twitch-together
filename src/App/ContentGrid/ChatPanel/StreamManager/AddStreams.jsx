import { useContext } from 'react';
import styled from 'styled-components';
import { baseElevation4, highlitGray, twitchPurple } from '../../../../globals';
import { store } from '../../../../store';

const StyledDiv = styled.div`
  align-items: center;
  background-color: ${ baseElevation4 };
  display: flex;
  flex-direction: column;
  padding: 0.25rem 0.5rem 0.5rem 0.5rem;
  width: 100%;
`;

const StyledLabel = styled.label`
  color: white;
  font-family: 'Roboto Condensed';
  font-size: 1.25rem;
`;

const StyledInput = styled.input`
  background-color: transparent;
  border: 0.1rem solid;
  border-color: ${ baseElevation4 } ${ baseElevation4 } ${ highlitGray };
  color: white;
  font-family: 'Roboto';
  font-size: 1rem;
  outline: none;
  padding: 0.25rem;
  width: 100%;

  &:focus {
    border-color: ${ baseElevation4 } ${ baseElevation4 } ${ twitchPurple };
  }
`;

export default function AddStreams() {
  const { dispatch } = useContext(store);
  const handleSubmit = e => {
    if (e.key === 'Enter' || e.key === 'NumpadEnter') {
      const value = e.target.value;

      if ( value.length > 0 ) {
        dispatch({ type: 'ADD_STREAMS', payload: value });
        e.target.value = '';
      }
    }
  };

  return (
    <StyledDiv>
      <StyledLabel for="add-streams">Add New Streams</StyledLabel>
      <StyledInput 
        id="add-streams"
        type="text"
        placeholder="Separate channel names using spaces"
        aria-placeholder="Separate channel names using spaces"
        onKeyDown={ handleSubmit }
      />
    </StyledDiv>
  );
}