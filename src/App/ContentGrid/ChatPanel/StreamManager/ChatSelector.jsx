import { useContext } from 'react';
import styled from 'styled-components';
import { Eye as EyeIcon } from '@styled-icons/bootstrap';
import { baseElevation3, twitchPurple, highlitGray } from '../../../../globals';
import { store } from '../../../../store';

const StreamButton = styled.button`
  background-color: ${ baseElevation3 };
  border: 0.2rem solid;
  border-color: ${ props => `${ baseElevation3 } ${ baseElevation3 } ${ props.isActive ? twitchPurple : highlitGray }` };
  color: white;
  cursor: pointer;
  display: flex;
  font-family: 'Roboto';
  font-size: 1.75rem;
  justify-content: space-between;
  margin: 0.5rem;
  outline: none;
  padding: 0.5rem;
  width: 75%;
  word-break: break-all;

  &:focus,
  &:hover {
    border-color: ${ props => props.isActive ? twitchPurple : highlitGray };
  }

  &:active {
    background-color: ${ highlitGray }
  }
`;

const Eye = styled(EyeIcon)`
  color: ${ props => props.isActive ? twitchPurple : highlitGray};
`;

export default function ChatSelector({ stream }) {
  const { state, dispatch } = useContext(store);
  const { activeChat } = state;

  return (
    <StreamButton
      isActive={ activeChat === stream }
      onClick={() => { 
        dispatch({ type: 'SET_ACTIVE_CHAT', payload: stream });
        dispatch({ type: 'SET_STREAM_MANAGER', payload: false });
      }}
    >
      <p>{ stream }</p>
      {/* <Icon
        id="eye"
        styling={{
          color: activeChat === stream ? twitchPurple : highlitGray,
          height: "32",
          weight: "32"
        }}
      /> */}
      <Eye isActive={ (activeChat === stream) } size="30"/>
    </StreamButton>
  );
}