import { useContext } from 'react';
import styled from 'styled-components';
import { animated } from 'react-spring';
import { store } from '../../../store';

const ChatEmbed = styled(animated.iframe)`
  border: 0 none;
  height: 100%;
  width: 100%;
`;

export default function TwitchChat({ key, transition }) {
  const { activeChat } = useContext(store).state;
  const srcUrl = `https://www.twitch.tv/embed/${activeChat}/chat?parent=${window.location.hostname}`;

  return (
      <ChatEmbed
        src={ srcUrl }
        key={ key }
        style={ transition }
      />
  );
}