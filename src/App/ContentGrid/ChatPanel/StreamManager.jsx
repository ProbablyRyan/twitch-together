import { useContext } from 'react';
import styled from 'styled-components';
import { animated } from 'react-spring';
import { store } from '../../../store';
import AddStreams from './StreamManager/AddStreams';
import ChatSelector from './StreamManager/ChatSelector';
import { baseGray, baseElevation4 } from '../../../globals';

const ManagerWrapper = styled(animated.div)`
  align-items: center;
  background-color: ${ baseGray };
  border: 0 none;
  display: flex;
  flex-direction: column;
  height: 90vh;
  overflow-x: hidden;
  overflow-y: auto;
  width: 100%;

  @media only screen and (max-width: 1050px) {
    height: 45vh;
  }
`;

const ManagerHeading = styled.h2`
  background-color: ${ baseElevation4 };
  color: white;
  font-family: 'Roboto Condensed';
  font-weight: normal;
  padding: 0.5rem;
  text-align: center;
  width: 100%;
`;

const StreamsWrapper = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  height: 100%;
  overflow-x: hidden;
  overflow-y: auto;
  width: 100%;
`;

export default function StreamManager({ key, transition }) {
  const { streams } = useContext(store).state;

  return (
    <ManagerWrapper
      key={ key }
      style={ transition }
    >
      <ManagerHeading>
        Choose Active Chat
      </ManagerHeading>
      <StreamsWrapper>
        {
          streams.map(stream =>
            <ChatSelector stream={ stream }/>
          )
        }
      </StreamsWrapper>
      <AddStreams/>
    </ManagerWrapper>
  );
}