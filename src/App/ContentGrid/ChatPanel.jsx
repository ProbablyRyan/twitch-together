import { useRef, useContext } from 'react';
import styled from 'styled-components';
import { animated, config, useChain, useTransition } from 'react-spring';
import { store } from '../../store';
import TwitchChat from './ChatPanel/TwitchChat';
import StreamManager from './ChatPanel/StreamManager';

export const PanelContainer = styled(animated.div)`
  background-color: white;
  flex: 1 0 350px;
  max-height: 90vh;
  min-height: 45vh;
  min-width: 350px;
  width: 100%;
`;

export default function ChatPanel({ key, transition }) {
  const { streamManagerIsVisible } = useContext(store).state;
  
  const chatTransitionRef = useRef();
  const managerTransitionRef = useRef();
  const transitionConfig = {
    config: config.stiff,
    from: {
      opacity: 0
    },
    enter: {
      opacity: 1
    },
    leave: {
      opacity: 0
    }
  };
  
  const chatTransition = useTransition(
    !(streamManagerIsVisible),
    null,
    {
      ...transitionConfig,
      ref: chatTransitionRef,
      initial: {
        opacity: 1
      }
    }
  );
  
  const managerTransition = useTransition(
    streamManagerIsVisible,
    null,
    {
      ...transitionConfig,
      ref: managerTransitionRef
    }
  );

  useChain(
    streamManagerIsVisible 
      ? [chatTransitionRef, managerTransitionRef] 
      : [managerTransitionRef, chatTransitionRef]
  );

  return (
    <PanelContainer
      key={ key }
      style={ transition }
    >
      {
        managerTransition.map(({ item, key, props }) =>
          item && <StreamManager key={ key } transition={ props } />
        )
      }
            {
        chatTransition.map(({ item, key, props }) =>
          item && <TwitchChat key={ key } transition={ props } />
        )
      }
    </PanelContainer>
  );
}