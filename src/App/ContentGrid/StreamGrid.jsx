import { useContext } from 'react';
import RGL from 'react-grid-layout';
import { SizeMe } from 'react-sizeme'
import styled from 'styled-components';
import { baseGray } from '../../globals';
import { store } from '../../store';

import '../../../node_modules/react-grid-layout/css/styles.css';
import '../../../node_modules/react-resizable/css/styles.css';

const StyledGrid = styled.div`
  background-color: ${ baseGray };
  flex: 8 1 auto;
  max-height: 90vh;
  min-height: 45vh;
  min-width: 700px;
  overflow-x: hidden;
  overflow-y: auto;

  @media only screen and (max-width: 1050px) {
    height: ${ props => props.chatPanelIsVisible ? "45vh" : "90vh"};
    min-width: 100%;
  }
`;

const GridLayout = styled(RGL)`
  width: 100%;
`;

const StyledGridItem = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0.5rem;
`;

const StyledHeading = styled.div`
  display: flex;
  flex: 0 0 auto;
  justify-content: space-between;

  &, & > * {
    color: white;
    font-family: 'Roboto';
    font-weight: bold;
  }

  & > span {
    cursor: grab;
    flex-grow: 1;
  }

  & > span:active {
    cursor: grabbing;
  }

  & > button {
    background: none;
    border: none;
    cursor: pointer;
    flex-grow: 0;
  }
`;

const GridItemHeader = ({ draggableHandle, stream }) => {
  const { dispatch } = useContext(store);

  return (
    <StyledHeading>
      <span
        className={ draggableHandle }
      >
        { stream }
      </span>
      <button
        onClick={ () => { dispatch({ type: 'REMOVE_STREAM', payload: stream }); } }
      >
        x
      </button>
  </StyledHeading>
  );
};

const TwitchStream = styled.iframe`
  border: none;
  flex-grow: 1;
  flex-shrink: 1;
`;

const GridItem = (stream) => {
  const srcUrl = `https://player.twitch.tv/?channel=${ stream }&parent=${window.location.hostname}&muted=true`;

  return (  
    <StyledGridItem
      key={ stream } 
      data-grid={{ x: 0, y: Infinity, w: 16, h: 5, minW: 2, minH: 4 }}
      >
      <GridItemHeader draggableHandle='MyDragHandleClassName' stream={ stream }/>
      <TwitchStream
        allowfullscreen="true"
        src={ srcUrl }
      />
    </StyledGridItem>
  );
};

export default function StreamGrid() {
  const { streams, chatPanelIsVisible } = useContext(store).state;
  return (
    <StyledGrid
      chatPanelIsVisible={ chatPanelIsVisible }
    >
      <SizeMe
        render={
          ({ size }) =>
            <GridLayout
              autoSize={ true }
              className="StreamGrid"
              cols={ 16 }
              draggableCancel=".MyDragCancel"
              draggableHandle=".MyDragHandleClassName"
              margin={[ 5, 5 ]}
              rowHeight= { 50 }
              width={ size.width }
            >
              {
                streams.map(GridItem)
              }
            </GridLayout>
        }
      />
    </StyledGrid>
  );
}
