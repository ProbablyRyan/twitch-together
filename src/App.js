import { useContext, useEffect } from 'react';
import BottomNav from './App/BottomNav';
import ContentGrid from './App/ContentGrid';
import queryString from 'query-string';
import { store } from './store';

function App() {
  const { dispatch } = useContext(store);
  
  useEffect(() => {
    const params = queryString.parse(window.location.search, { arrayFormat: 'comma' });
    dispatch({ type: 'ADD_STREAMS', payload: params.channels });
  }, []);

  return (
    <>
      <ContentGrid/>
      <BottomNav/>
    </>
  );
}

export default App;
