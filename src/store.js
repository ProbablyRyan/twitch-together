import { createContext, useReducer } from 'react';
import { uniq } from 'lodash';

const initialState = {
  activeChat: '',
  chatPanelIsVisible: true,
  streamManagerIsVisible: true,
  streams: []
};
const store = createContext(initialState);
const ContextProvider = store.Provider;

const handlers = {
  'ADD_STREAMS': oldState => payload => { 
    const streamsToAdd = payload
      ? Array.isArray(payload)
        // If payload is an array streamsToAdd === payload
        ? payload
        // Else, streamsToAdd is the result of converting payload to an array of strings
        : String(payload).split(' ')
      // If payload is undefined streamsToAdd is an empty array
      : [];

    const newStreams = oldState
      .streams
      .concat(
        // Remove duplicate streams
        uniq(streamsToAdd)
        // Remove previously added streams
        .filter( stream => !oldState.streams.includes(stream) )
      )
      // Sort resulting array alphabetically
      .sort(( a, b ) => {
        const x = a.toLowerCase();
        const y = b.toLowerCase();

        return x.localeCompare(y);
      });

    return {
      ...oldState,
      activeChat: !Boolean(oldState.activeChat)
        ? newStreams[0]
        : oldState.activeChat,
      streams: newStreams
    };
  },

  'REMOVE_STREAM': oldState => streamToRemove => {
    const newStreams = oldState.streams.filter( stream => stream !== streamToRemove );

    return {
      ...oldState,
      activeChat: oldState.activeChat === streamToRemove
        ? newStreams.length > 0
          ? newStreams[0]
          : ''
        : oldState.activeChat,
      streams: newStreams,
      streamManagerIsVisible: newStreams.length > 0 ? oldState.streamManagerIsVisible : true
    };
  },

  'SET_ACTIVE_CHAT': oldState => chat => ({ ...oldState, activeChat: chat }),

  'SET_STREAM_MANAGER': oldState => managerSetting => ({ 
    ...oldState, 
    streamManagerIsVisible: managerSetting,
    chatPanelIsVisible: true
  }),

  'TOGGLE_CHAT_PANEL': oldState => () => ({ ...oldState, chatPanelIsVisible: !oldState.chatPanelIsVisible })
};

function Provider({ children }) {
  const [state, dispatch] = useReducer(
    (oldState, { type, payload }) => {
      const handler = handlers[type];
      return handler
        ? handler(oldState)(payload)
        : { ...oldState }
    },
    initialState
  );

  return (
    <ContextProvider value={{ state, dispatch }}>
      { children }
    </ContextProvider>
  );
}

export { store, Provider }