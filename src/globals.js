import Color from 'color';
import { createGlobalStyle } from 'styled-components';

export const
  baseGray = Color('#121212'),
  highlitGray = baseGray.lighten(2),
  baseElevation2 = baseGray.lighten(0.7),
  baseElevation3 = baseGray.lighten(0.85),
  baseElevation4 = baseGray.lighten(1),
  twitchPurple = '#9147ff';

export const GlobalStyles = createGlobalStyle`
  /* React-Grid-Layout overrides */
  .react-grid-layout {
    position: static;
  }

  .react-grid-item.react-grid-placeholder {
    background: ${ twitchPurple };
  }

  .react-grid-item > .react-resizable-handle::after {
    border-right: 0.5rem solid white;
    border-bottom: 0.5rem solid white;
    bottom: 0;
    height: 20px;
    right: 0;
    transform: scale(0.5);
    width: 20px;
  }

  .react-draggable-transparent-selection {
  span, div, a {
    &::selection {
      background: transparent;
    }
  }
}

  /* CSS Reset */
  * {
    box-sizing: border-box;
    font-family: Roboto, sans-serif;
    margin: 0;
    padding: 0;
    -webkit-tap-highlight-color: transparent;
  }
  
  html {
    background-color: ${ baseGray };
    line-height: 1.15;
    -webkit-text-size-adjust: 100%; 
  }

  body {
    margin: 0;
  }

  main {
    display: block;
  }
  
  h1 {
    font-size: 2em;
    margin: 0.67em 0;
  }
  
  hr {
    box-sizing: content-box;
    height: 0;
    overflow: visible;
  }
  
  pre {
    font-family: monospace, monospace;
    font-size: 1em;
  }
  
  a {
    background-color: transparent;
  }
  
  abbr[title] {
    border-bottom: none;
    text-decoration: underline;
    text-decoration: underline dotted;
  }
  
  b,
  strong {
    font-weight: bolder;
  }
  
  code,
  kbd,
  samp {
    font-family: monospace, monospace;
    font-size: 1em;
  }
  
  small {
    font-size: 80%;
  }
  
  sub,
  sup {
    font-size: 75%;
    line-height: 0;
    position: relative;
    vertical-align: baseline;
  }
  
  sub {
    bottom: -0.25em;
  }
  
  sup {
    top: -0.5em;
  }
  
  img {
    border-style: none;
  }
  
  button,
  input,
  optgroup,
  select,
  textarea {
    font-family: inherit;
    font-size: 100%;
    line-height: 1.15;
    margin: 0;
  }
  
  button,
  input {
    overflow: visible;
  }
  
  button,
  select {
    text-transform: none;
  }
  
  button,
  [type="button"],
  [type="reset"],
  [type="submit"] {
    -webkit-appearance: button;
  }
  
  button::-moz-focus-inner,
  [type="button"]::-moz-focus-inner,
  [type="reset"]::-moz-focus-inner,
  [type="submit"]::-moz-focus-inner {
    border-style: none;
    padding: 0;
  }
  
  button:-moz-focusring,
  [type="button"]:-moz-focusring,
  [type="reset"]:-moz-focusring,
  [type="submit"]:-moz-focusring {
    outline: 1px dotted ButtonText;
  }
  
  fieldset {
    padding: 0.35em 0.75em 0.625em;
  }
  
  legend {
    box-sizing: border-box;
    color: inherit;
    display: table;
    max-width: 100%;
    padding: 0;
    white-space: normal;
  }
  
  progress {
    vertical-align: baseline;
  }
  
  textarea {
    overflow: auto;
  }
  
  [type="checkbox"],
  [type="radio"] {
    box-sizing: border-box;
    padding: 0;
  }
  
  [type="number"]::-webkit-inner-spin-button,
  [type="number"]::-webkit-outer-spin-button {
    height: auto;
  }
  
  [type="search"] {
    -webkit-appearance: textfield;
    outline-offset: -2px;
  }
  
  [type="search"]::-webkit-search-decoration {
    -webkit-appearance: none;
  }
  
  ::-webkit-file-upload-button {
    -webkit-appearance: button;
    font: inherit;
  }
  
  details {
    display: block;
  }
  
  summary {
    display: list-item;
  }
  
  template {
    display: none;
  }
  
  [hidden] {
    display: none;
  }  
`;